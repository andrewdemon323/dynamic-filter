﻿using System.Data.Entity;
using System.Data.Entity.Migrations.Model;
using System.Linq;

namespace DAL.Repo
{
    public class Repo<T> where T : class
    {
        private AppDbContext _context;
        private DbSet<T> _dbSet;

        public Repo(AppDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public DbSet<T> Get()
        {
            return _dbSet;
        }

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
        }
    }
}