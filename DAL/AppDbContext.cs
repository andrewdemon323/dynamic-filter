﻿using System.Data.Entity;
using DAL.Entities;
using DAL.Entities.EAV;
using DAL.Entities.EAV.Attributes;
using DAL.Entities.EAV.Values;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        static AppDbContext()
        {
            Database.SetInitializer<AppDbContext>(new InitDb());
        }
        public AppDbContext() : base("DynamicSearch")
        {}

        public DbSet<Product> Products { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<AttributeInt> AttributesInt { get; set; }
        public DbSet<AttributeIntValue> AttributeIntValues { get; set; }
        public DbSet<AttributeDecimal> AttributesDecimal { get; set; }
        public DbSet<AttributeDecimalValue> AttributeDecimalValues { get; set; }
        public DbSet<AttributeText> AttributesText { get; set; }
        public DbSet<AttributeTextValue> AttributeTextValues { get; set; }
        public DbSet<AttributeOption> AttributesOption { get; set; }
        public DbSet<AttributeOptionValue> AttributeOptionValues { get; set; }
        public DbSet<Option> Options { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Entity>()
                .HasMany(e => e.Products)
                .WithRequired(p => p.Entity).WillCascadeOnDelete(false);
            modelBuilder.Entity<Entity>()
                .HasMany(e => e.IntAttributes)
                .WithRequired(a => a.Entity).WillCascadeOnDelete(false);
            modelBuilder.Entity<Entity>()
                .HasMany(e => e.DecimalAttributes)
                .WithRequired(a => a.Entity).WillCascadeOnDelete(false);
            modelBuilder.Entity<Entity>()
                .HasMany(e => e.TextAttributes)
                .WithRequired(a => a.Entity).WillCascadeOnDelete(false);
            modelBuilder.Entity<Entity>()
                .HasMany(e => e.OptionAttributes)
                .WithRequired(a => a.Entity).WillCascadeOnDelete(false);

            modelBuilder.Entity<Product>()
                .HasRequired(p => p.Entity)
                .WithMany(e => e.Products);
            modelBuilder.Entity<Product>()
                .HasMany(p => p.IntValues)
                .WithRequired(v => v.Product).WillCascadeOnDelete(true);
            modelBuilder.Entity<Product>()
                .HasMany(p => p.DecimalValues)
                .WithRequired(v => v.Product).WillCascadeOnDelete(true);
            modelBuilder.Entity<Product>()
                .HasMany(p => p.TextValues)
                .WithRequired(v => v.Product).WillCascadeOnDelete(true);
            modelBuilder.Entity<Product>()
                .HasMany(p => p.OptionValues)
                .WithRequired(v => v.Product).WillCascadeOnDelete(true);

            modelBuilder.Entity<AttributeInt>()
                .HasMany(a => a.Values)
                .WithRequired(v => v.Attribute).WillCascadeOnDelete(false);
            modelBuilder.Entity<AttributeInt>()
                .HasRequired(a => a.Entity)
                .WithMany(e => e.IntAttributes);
            modelBuilder.Entity<AttributeIntValue>()
                .HasRequired(av => av.Attribute)
                .WithMany(a => a.Values);
            modelBuilder.Entity<AttributeIntValue>()
                .HasRequired(av => av.Product)
                .WithMany(p => p.IntValues);

            modelBuilder.Entity<AttributeDecimal>()
                .HasMany(a => a.Values)
                .WithRequired(v => v.Attribute).WillCascadeOnDelete(false);
            modelBuilder.Entity<AttributeDecimal>()
                .HasRequired(a => a.Entity)
                .WithMany(e => e.DecimalAttributes);
            modelBuilder.Entity<AttributeDecimalValue>()
                .HasRequired(av => av.Attribute)
                .WithMany(a => a.Values);
            modelBuilder.Entity<AttributeDecimalValue>()
                .HasRequired(av => av.Product)
                .WithMany(p => p.DecimalValues);

            modelBuilder.Entity<AttributeText>()
                .HasMany(a => a.Values)
                .WithRequired(v => v.Attribute).WillCascadeOnDelete(false);
            modelBuilder.Entity<AttributeText>()
                .HasRequired(a => a.Entity)
                .WithMany(e => e.TextAttributes);
            modelBuilder.Entity<AttributeTextValue>()
                .HasRequired(av => av.Attribute)
                .WithMany(a => a.Values);
            modelBuilder.Entity<AttributeTextValue>()
                .HasRequired(av => av.Product)
                .WithMany(p => p.TextValues);

            modelBuilder.Entity<AttributeOption>()
                .HasMany(a => a.Values)
                .WithRequired(v => v.Attribute).WillCascadeOnDelete(false);
            modelBuilder.Entity<AttributeOption>()
                .HasRequired(a => a.Entity)
                .WithMany(e => e.OptionAttributes);
            modelBuilder.Entity<AttributeOption>()
                .HasMany(a => a.Options)
                .WithRequired(o => o.AttributeOption).WillCascadeOnDelete(false);
            modelBuilder.Entity<AttributeOptionValue>()
                .HasRequired(av => av.Attribute)
                .WithMany(a => a.Values);
            modelBuilder.Entity<AttributeOptionValue>()
                .HasRequired(av => av.Product)
                .WithMany(p => p.OptionValues);
            
            modelBuilder.Entity<Option>()
                .HasRequired(o => o.AttributeOption)
                .WithMany(a => a.Options);
        }
    }
}