﻿using System.Data.Entity;
using DAL.Entities;
using DAL.Entities.EAV;
using DAL.Entities.EAV.Attributes;

namespace DAL
{
    public class InitDb : CreateDatabaseIfNotExists<AppDbContext>
    {
        protected override void Seed(AppDbContext db)
        {
            Entity e = new Entity {Name = "Book"};

            AttributeText aText = new AttributeText{Name = "Name", EntityId = e.Id};
            e.TextAttributes.Add(aText);

            AttributeText author = new AttributeText { Name = "Author", EntityId = e.Id };
            e.TextAttributes.Add(author);

            AttributeInt pageCount = new AttributeInt
            {
                Name = "Count Pages",
                MinVal = 0,
                MaxVal = 1000,
                EntityId = e.Id
            };
            e.IntAttributes.Add(pageCount);

            AttributeDecimal price = new AttributeDecimal
            {
                Name = "Price",
                MinVal = 0,
                MaxVal = 9999,
                EntityId = e.Id
            };
            e.DecimalAttributes.Add(price);

            AttributeOption genre = new AttributeOption{Name = "Genre",EntityId = e.Id};
            string[] genres = {"science", "fantazy", "literature"};
            foreach (string g in genres)
            {
                Option o = new Option() {Name = g};
                genre.Options.Add(o);
            }
            e.OptionAttributes.Add(genre);

            db.Entities.Add(e);
            db.SaveChanges();

        }
    }
}