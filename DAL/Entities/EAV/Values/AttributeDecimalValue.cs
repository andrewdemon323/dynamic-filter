﻿using DAL.Entities.EAV.Attributes;

namespace DAL.Entities.EAV.Values
{
    public class AttributeDecimalValue
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public int ProductId { get; set; }
        public decimal Val { get; set; }
        public virtual AttributeDecimal Attribute { get; set; }
        public virtual Product Product { get; set; }
    }
}