﻿using DAL.Entities.EAV.Attributes;

namespace DAL.Entities.EAV.Values
{
    public interface IValue : IEntity
    {
        int AttributeId { get; set; }
        int ProductId { get; set; }
        Product Product { get; set; }
    }
}