﻿using DAL.Entities.EAV.Attributes;

namespace DAL.Entities.EAV.Values
{
    public class AttributeIntValue : IValue
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public int ProductId { get; set; }
        public int Val { get; set; }
        public virtual AttributeInt Attribute { get; set; }
        public virtual Product Product { get; set; }
    }
}