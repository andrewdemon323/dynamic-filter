﻿using DAL.Entities.EAV.Attributes;

namespace DAL.Entities.EAV.Values
{
    public class AttributeTextValue : IValue
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public int ProductId { get; set; }
        public string Val { get; set; }
        public virtual AttributeText Attribute { get; set; }
        public virtual Product Product { get; set; }
    }
}