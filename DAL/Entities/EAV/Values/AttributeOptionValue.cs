﻿using DAL.Entities.EAV.Attributes;

namespace DAL.Entities.EAV.Values
{
    public class AttributeOptionValue : IValue
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public int ProductId { get; set; }
        public int ValId { get; set; }
        public virtual Option Val { get; set; }
        public virtual AttributeOption Attribute { get; set; }
        public virtual Product Product { get; set; }
    }
}