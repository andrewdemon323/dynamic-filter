﻿using System.Collections;
using System.Collections.Generic;
using DAL.Entities.EAV.Attributes;

namespace DAL.Entities.EAV
{
    public class Entity : IEntity
    {
        public Entity()
        {
            Products = new List<Product>();
            IntAttributes = new List<AttributeInt>();
            DecimalAttributes = new List<AttributeDecimal>();
            TextAttributes = new List<AttributeText>();
            OptionAttributes = new List<AttributeOption>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<AttributeInt> IntAttributes { get; set; }
        public virtual ICollection<AttributeDecimal> DecimalAttributes { get; set; }
        public virtual ICollection<AttributeText> TextAttributes { get; set; }
        public virtual ICollection<AttributeOption> OptionAttributes { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}