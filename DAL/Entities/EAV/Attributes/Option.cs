﻿namespace DAL.Entities.EAV.Attributes
{
    public class Option : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AttributeOptionId { get; set; }
        public virtual AttributeOption AttributeOption { get; set; }
    }
}