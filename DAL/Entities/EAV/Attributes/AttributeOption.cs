﻿using System.Collections.Generic;
using DAL.Entities.EAV.Values;

namespace DAL.Entities.EAV.Attributes
{
    public class AttributeOption : IAttribute
    {
        public AttributeOption()
        {
            Options = new List<Option>();
            Values = new List<AttributeOptionValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int EntityId { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual List<Option> Options { get; set; }
        public virtual ICollection<AttributeOptionValue> Values { get; set; }
    }
}