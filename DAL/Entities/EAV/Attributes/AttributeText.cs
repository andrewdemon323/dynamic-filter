﻿using System.Collections.Generic;
using DAL.Entities.EAV.Values;

namespace DAL.Entities.EAV.Attributes
{
    public class AttributeText : IAttribute
    {
        public AttributeText()
        {
            Values = new List<AttributeTextValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int EntityId { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual ICollection<AttributeTextValue> Values { get; set; }
    }
}