﻿using System.Collections.Generic;
using DAL.Entities.EAV.Values;

namespace DAL.Entities.EAV.Attributes
{
    public class AttributeDecimal
    {
        public AttributeDecimal()
        {
            Values = new List<AttributeDecimalValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal MinVal { get; set; }
        public decimal MaxVal { get; set; }
        public int EntityId { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual ICollection<AttributeDecimalValue> Values { get; set; }
    }
}