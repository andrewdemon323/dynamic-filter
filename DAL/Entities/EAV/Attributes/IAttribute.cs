﻿namespace DAL.Entities.EAV.Attributes
{
    public interface IAttribute : IEntity
    {
        string Name { get; set; }
        int EntityId { get; set; }
        Entity Entity { get; set; }
    }
}