﻿using System.Collections.Generic;
using DAL.Entities.EAV.Values;

namespace DAL.Entities.EAV.Attributes
{
    public class AttributeInt : IAttribute
    {
        public AttributeInt()
        {
            Values = new List<AttributeIntValue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int MinVal { get; set; }
        public int MaxVal { get; set; }
        public int EntityId { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual ICollection<AttributeIntValue> Values { get; set; }
    }
}