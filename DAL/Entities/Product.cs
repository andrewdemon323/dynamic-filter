﻿using System.Collections.Generic;
using System.Text;
using DAL.Entities.EAV;
using DAL.Entities.EAV.Values;

namespace DAL.Entities
{
    public class Product : IEntity, IProduct
    {
        public Product()
        {
            IntValues = new List<AttributeIntValue>();
            DecimalValues = new List<AttributeDecimalValue>();
            TextValues = new List<AttributeTextValue>();
            OptionValues = new List<AttributeOptionValue>();
        }

        public int Id { get; set; }
        public int EntityId { get; set; }
        public virtual Entity Entity { get; set; }
        public virtual ICollection<AttributeIntValue> IntValues { get; set; }
        public virtual ICollection<AttributeDecimalValue> DecimalValues { get; set; }
        public virtual ICollection<AttributeTextValue> TextValues { get; set; }
        public virtual ICollection<AttributeOptionValue> OptionValues { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{Entity.Name}:\n");
            foreach (var value in IntValues)
            {
                sb.Append($"{value.Attribute.Name} -- {value.Val}\n");
            }
            foreach (var value in TextValues)
            {
                sb.Append($"{value.Attribute.Name} -- {value.Val}\n");
            }
            foreach (var value in DecimalValues)
            {
                sb.Append($"{value.Attribute.Name} -- {value.Val}\n");
            }
            foreach (var value in OptionValues)
            {
                sb.Append($"{value.Attribute.Name} -- {value.Val.Name}\n");
            }
            return sb.ToString();
        }
    }
}