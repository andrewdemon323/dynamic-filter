﻿using DAL.Entities.EAV;

namespace DAL.Entities
{
    public interface IProduct
    {
        int EntityId { get; set; }
        Entity Entity { get; set; }
    }
}