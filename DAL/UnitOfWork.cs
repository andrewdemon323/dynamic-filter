﻿using System;
using System.Threading.Tasks;
using DAL.Entities;
using DAL.Entities.EAV;
using DAL.Entities.EAV.Attributes;
using DAL.Entities.EAV.Values;
using DAL.Repo;

namespace DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly AppDbContext _context = new AppDbContext();
        private Repo<Product> _productRepo;
        private Repo<Entity> _entityRepo;
        private Repo<AttributeInt> _attrIntRepo;
        private Repo<AttributeIntValue> _attrIntValRepo;
        private Repo<AttributeDecimal> _attrDecimalRepo;
        private Repo<AttributeDecimalValue> _attrDecimalValRepo;
        private Repo<AttributeText> _attrTextRepo;
        private Repo<AttributeTextValue> _attrTextValRepo;
        private Repo<AttributeOption> _attrOptionRepo;
        private Repo<AttributeOptionValue> _attrOptionValRepo;
        private Repo<Option> _optionRepo;

        public Repo<Product> ProductRepo => _productRepo?? (_productRepo = new Repo<Product>(_context));
        public Repo<Entity> EntityRepo => _entityRepo ?? (_entityRepo = new Repo<Entity>(_context));
        public Repo<AttributeInt> AttrIntRepo => _attrIntRepo?? (_attrIntRepo= new Repo<AttributeInt>(_context));
        public Repo<AttributeIntValue> AttrIntValRepo => _attrIntValRepo?? (_attrIntValRepo=new Repo<AttributeIntValue>(_context));
        public Repo<AttributeDecimal> AttrDecimalRepo => _attrDecimalRepo?? (_attrDecimalRepo= new Repo<AttributeDecimal>(_context));
        public Repo<AttributeDecimalValue> AttrDecimalValRepo => _attrDecimalValRepo?? (_attrDecimalValRepo = new Repo<AttributeDecimalValue>(_context));
        public Repo<AttributeText> AttrTextRepo => _attrTextRepo?? (_attrTextRepo = new Repo<AttributeText>(_context));
        public Repo<AttributeTextValue> AttrTextValRepo => _attrTextValRepo?? (_attrTextValRepo= new Repo<AttributeTextValue>(_context));
        public Repo<AttributeOption> AttrOptionRepo => _attrOptionRepo?? (_attrOptionRepo = new Repo<AttributeOption>(_context));
        public Repo<AttributeOptionValue> AttrOptionValRepo => _attrOptionValRepo?? (_attrOptionValRepo = new Repo<AttributeOptionValue>(_context));
        public Repo<Option> OptionRepo => _optionRepo ?? (_optionRepo = new Repo<Option>(_context));

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}