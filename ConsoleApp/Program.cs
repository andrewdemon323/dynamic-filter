﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DAL;
using DAL.Entities;
using DAL.Entities.EAV;
using DAL.Entities.EAV.Attributes;
using DAL.Entities.EAV.Values;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            UnitOfWork db = new UnitOfWork();
            var carEnt = db.EntityRepo.Get().FirstOrDefault(e => e.Name == "Book");
            if (carEnt == null)
            {
                Console.WriteLine("Entity not found");
            }
            else
            {
                Product p = new Product();
                var textAttrs = carEnt.TextAttributes;
                foreach (var attr in textAttrs)
                {
                    var attrVal = new AttributeTextValue()
                    {
                        AttributeId = attr.Id,
                        Val = "Harry Potter"
                    };
                    p.TextValues.Add(attrVal);
                }
                var decAttrs = carEnt.DecimalAttributes;
                foreach (var attr in decAttrs)
                {
                    var attrVal = new AttributeDecimalValue()
                    {
                        AttributeId = attr.Id,
                        Val = 300.00m
                    };
                    p.DecimalValues.Add(attrVal);
                }
                var optAttrs = carEnt.OptionAttributes;
                foreach (var attr in optAttrs)
                {
                    var attrVal = new AttributeOptionValue()
                    {
                        AttributeId = attr.Id,
                        Val = attr.Options.FirstOrDefault(o => o.Name=="white")
                    };
                    p.OptionValues.Add(attrVal);
                }
                carEnt.Products.Add(p);
                db.Save();

                Console.WriteLine(p.ToString());
            }
            Console.ReadKey();
        }
    }
}
