﻿using System.Collections.Generic;
using DAL.Entities.EAV.Attributes;

namespace BLL.Util
{
    public class OptionCriteria
    {
        public OptionCriteria()
        {
            //Vals = new List<Option>();
            ValIds = new int[]{};
        }

        public int AttributeId { get; set; }
        public string Name { get; set; }
        //public List<Option> Vals { get; set; }
        public int[] ValIds { get; set; }
    }
}