﻿namespace BLL.Util
{
    public class TextCriteria
    {
        public int AttributeId { get; set; }
        public string Name { get; set; }
        public string Val { get; set; }
    }
}