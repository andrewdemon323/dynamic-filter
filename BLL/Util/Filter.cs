﻿using System.Collections.Generic;
using System.Linq;
using DAL.Entities;

namespace BLL.Util
{
    public class Filter
    {
        private IQueryable<Product> _products;

        public Filter(IQueryable<Product> products)
        {
            _products = products;
        }

        public List<Product> Filtrate(Criteria crit)
        {
            if (crit != null)
            {
                if (crit.TextCriterias?.Count > 0)
                {
                    foreach (var criteria in crit.TextCriterias)
                    {
                        if (criteria.Val != null)
                            _products = _products.Where(p =>
                                p.TextValues.Any(
                                    v => v.AttributeId == criteria.AttributeId
                                         && v.Val.StartsWith(criteria.Val)));
                    }
                }
                if (crit.IntCriterias?.Count > 0)
                {
                    foreach (var criteria in crit.IntCriterias)
                    {
                        _products = _products.Where(p =>
                            p.IntValues.Any(
                                v => v.AttributeId == criteria.AttributeId
                                     && v.Val >= criteria.MinVal));
                        if (criteria.MaxVal > 0)
                            _products = _products.Where(p =>
                                p.IntValues.Any(v => v.AttributeId == criteria.AttributeId
                                                     && v.Val <= criteria.MaxVal));
                    }
                }
                if (crit.DecimalCriterias?.Count > 0)
                {
                    foreach (var criteria in crit.DecimalCriterias)
                    {
                        _products = _products.Where(p =>
                            p.DecimalValues.Any(
                                v => v.AttributeId == criteria.AttributeId
                                     && v.Val >= criteria.MinVal));
                        if (criteria.MaxVal > 0)
                            _products = _products.Where(p =>
                                p.DecimalValues.Any(
                                    v => v.AttributeId == criteria.AttributeId
                                         && v.Val <= criteria.MaxVal));
                    }
                }
                if (crit.OptionCriterias?.Count > 0)
                {
                    foreach (var criteria in crit.OptionCriterias)
                    {
                        if (criteria.ValIds.Length > 0)
                        {
                            _products = _products.ToList().AsQueryable().Where(p =>
                                p.OptionValues.Any(v => v.AttributeId == criteria.AttributeId
                                                        && criteria.ValIds.Any(val => val == v.Val.Id)));
                        }
                    }
                }
            }
            return _products.ToList();
        }
    }
}