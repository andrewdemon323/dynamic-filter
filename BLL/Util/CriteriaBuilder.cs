﻿using DAL.Entities.EAV;

namespace BLL.Util
{
    public class CriteriaBuilder
    {
        public static Criteria BuildCriteria(Entity e)
        {
            Criteria c = new Criteria();
            foreach (var attr in e.TextAttributes)
            {
                var crit = new TextCriteria
                {
                    Name = attr.Name,
                    AttributeId = attr.Id,
                    Val = ""
                };
                c.TextCriterias.Add(crit);
            }
            foreach (var attr in e.IntAttributes)
            {
                var crit = new IntCriteria
                {
                    Name = attr.Name,
                    AttributeId = attr.Id,
                    MinVal = attr.MinVal,
                    MaxVal = attr.MaxVal
                };
                c.IntCriterias.Add(crit);
            }

            foreach (var attr in e.DecimalAttributes)
            {
                var crit = new DecimalCriteria
                {
                    Name = attr.Name,
                    AttributeId = attr.Id,
                    MinVal = attr.MinVal,
                    MaxVal = attr.MaxVal
                };
                c.DecimalCriterias.Add(crit);
            }

            foreach (var attr in e.OptionAttributes)
            {
                var crit = new OptionCriteria
                {
                    Name = attr.Name,
                    AttributeId = attr.Id,
                };
                c.OptionCriterias.Add(crit);
            }
            return c;
        }
    }
}