﻿using System.Collections.Generic;

namespace BLL.Util
{
    public class Criteria
    {
        public Criteria()
        {
            TextCriterias = new List<TextCriteria>();
            IntCriterias = new List<IntCriteria>();
            DecimalCriterias = new List<DecimalCriteria>();
            OptionCriterias = new List<OptionCriteria>();
        }

        public List<TextCriteria> TextCriterias { get; set; }
        public List<IntCriteria> IntCriterias { get; set; }
        public List<DecimalCriteria> DecimalCriterias { get; set; }
        public List<OptionCriteria> OptionCriterias { get; set; }
    }
}