﻿namespace BLL.Util
{
    public class DecimalCriteria
    {
        public int AttributeId { get; set; }
        public string Name { get; set; }
        public decimal MinVal { get; set; }
        public decimal MaxVal { get; set; }
    }
}