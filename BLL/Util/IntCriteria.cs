﻿namespace BLL.Util
{
    public class IntCriteria
    {
        public int AttributeId { get; set; }
        public string Name { get; set; }
        public int MinVal { get; set; }
        public int MaxVal { get; set; }
    }
}