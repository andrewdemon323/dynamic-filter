﻿using System.Collections.Generic;
using BLL.DTOs.EAV.ValuesDTOs;

namespace BLL.DTOs
{
    public class EntityDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}