﻿namespace BLL.DTOs.EAV.ValuesDTOs
{
    public class OptionValueDto
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public int OptionId { get; set; }
        public OptionDto Option { get; set; }
    }
}