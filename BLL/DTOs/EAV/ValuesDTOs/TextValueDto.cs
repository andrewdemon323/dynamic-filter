﻿namespace BLL.DTOs.EAV.ValuesDTOs
{
    public class TextValueDto
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public string Val { get; set; }
    }
}