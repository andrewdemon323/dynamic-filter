﻿namespace BLL.DTOs.EAV.ValuesDTOs
{
    public class OptionDto
    {
        public int Id { get; set; }
        public string Val { get; set; }
    }
}