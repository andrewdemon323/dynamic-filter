﻿namespace BLL.DTOs.EAV.ValuesDTOs
{
    public class IntValueDto
    {
        public int Id { get; set; }
        public int AttributeId { get; set; }
        public int Val { get; set; }
    }
}