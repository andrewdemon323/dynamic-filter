﻿using System.Collections.Generic;
using BLL.DTOs.EAV.ValuesDTOs;

namespace BLL.DTOs
{
    public class ProductDto
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public EntityDto Entity { get; set; }
        public IEnumerable<IntValueDto> IntVals { get; set; }
        public IEnumerable<TextValueDto> TextVals { get; set; }
        public IEnumerable<OptionValueDto> OptionVals { get; set; }
    }
}