﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Entities.EAV.Attributes;

namespace DynamicSearch.Controllers
{
    public class OptionsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: Options
        public async Task<ActionResult> Index()
        {
            var options = db.Options.Include(o => o.AttributeOption);
            return View(await options.ToListAsync());
        }

        // GET: Options/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Option option = await db.Options.FindAsync(id);
            if (option == null)
            {
                return HttpNotFound();
            }
            return View(option);
        }

        // GET: Options/Create
        public ActionResult Create()
        {
            ViewBag.AttributeOptionId = new SelectList(db.AttributesOption, "Id", "Name");
            return View();
        }

        // POST: Options/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,AttributeOptionId")] Option option)
        {
            if (ModelState.IsValid)
            {
                db.Options.Add(option);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AttributeOptionId = new SelectList(db.AttributesOption, "Id", "Name", option.AttributeOptionId);
            return View(option);
        }

        // GET: Options/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Option option = await db.Options.FindAsync(id);
            if (option == null)
            {
                return HttpNotFound();
            }
            ViewBag.AttributeOptionId = new SelectList(db.AttributesOption, "Id", "Name", option.AttributeOptionId);
            return View(option);
        }

        // POST: Options/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,AttributeOptionId")] Option option)
        {
            if (ModelState.IsValid)
            {
                db.Entry(option).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.AttributeOptionId = new SelectList(db.AttributesOption, "Id", "Name", option.AttributeOptionId);
            return View(option);
        }

        // GET: Options/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Option option = await db.Options.FindAsync(id);
            if (option == null)
            {
                return HttpNotFound();
            }
            return View(option);
        }

        // POST: Options/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Option option = await db.Options.FindAsync(id);
            db.Options.Remove(option);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
