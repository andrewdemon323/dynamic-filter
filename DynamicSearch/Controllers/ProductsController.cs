﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL.Util;
using DAL;
using DAL.Entities;
using DAL.Entities.EAV.Values;

namespace DynamicSearch.Controllers
{
    public class ProductsController : Controller
    {
        private UnitOfWork db = new UnitOfWork();
        // GET: Products
        public async Task<ActionResult> Index(Criteria crit)
        {
            var products = db.ProductRepo.Get().AsQueryable();
            BLL.Util.Filter filter = new BLL.Util.Filter(products);
            var result = filter.Filtrate(crit);
            var entity = await db.EntityRepo.Get().FirstOrDefaultAsync(e => e.Name == "Book");
            ViewBag.Entity = entity;
            if (crit.DecimalCriterias.Count == 0
                && crit.OptionCriterias.Count == 0
                && crit.IntCriterias.Count == 0
                && crit.TextCriterias.Count == 0
            )
            {
                ViewBag.Criteria = CriteriaBuilder.BuildCriteria(entity);
            }
            else
            {
                ViewBag.Criteria = crit;
            }
            return View(result);
        }

        // GET: Products/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.ProductRepo.Get().FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            var entity = db.EntityRepo.Get().FirstOrDefault(e => e.Name == "Book");
            var product = new Product();
            product.Entity = entity;
            foreach (var attribute in entity.TextAttributes)
            {
                product.TextValues.Add(new AttributeTextValue {Attribute = attribute, Val = ""});
            }
            foreach (var attribute in entity.IntAttributes)
            {
                product.IntValues.Add(new AttributeIntValue {Attribute = attribute, Val = 0});
            }
            foreach (var attribute in entity.DecimalAttributes)
            {
                product.DecimalValues.Add(new AttributeDecimalValue {Attribute = attribute, Val = 0.0m});
            }
            foreach (var attribute in entity.OptionAttributes)
            {
                product.OptionValues.Add(new AttributeOptionValue
                {
                    Attribute = attribute,
                    Val = attribute.Options.First()
                });
            }
            return View(product);
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Product product)
        {
            if (ModelState.IsValid)
            {
                db.ProductRepo.Add(product);
                await db.SaveAsync();
                return RedirectToAction("Index");
            }
            ViewBag.EntityId = new SelectList(db.EntityRepo.Get(), "Id", "Name", product.EntityId);
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.ProductRepo.Get().FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.EntityId = new SelectList(db.EntityRepo.Get(), "Id", "Name", product.EntityId);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,EntityId")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.ProductRepo.Update(product);
                await db.SaveAsync();
                return RedirectToAction("Index");
            }
            ViewBag.EntityId = new SelectList(db.EntityRepo.Get(), "Id", "Name", product.EntityId);
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = await db.ProductRepo.Get().FindAsync(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Product product = await db.ProductRepo.Get().FindAsync(id);
            db.ProductRepo.Delete(product);
            await db.SaveAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db?.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
