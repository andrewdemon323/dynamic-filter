﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Entities.EAV.Attributes;

namespace DynamicSearch.Controllers
{
    public class AttributeDecimalsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: AttributeDecimals
        public async Task<ActionResult> Index()
        {
            var attributesDecimal = db.AttributesDecimal.Include(a => a.Entity);
            return View(await attributesDecimal.ToListAsync());
        }

        // GET: AttributeDecimals/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeDecimal attributeDecimal = await db.AttributesDecimal.FindAsync(id);
            if (attributeDecimal == null)
            {
                return HttpNotFound();
            }
            return View(attributeDecimal);
        }

        // GET: AttributeDecimals/Create
        public ActionResult Create()
        {
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name");
            return View();
        }

        // POST: AttributeDecimals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,MinVal,MaxVal,EntityId")] AttributeDecimal attributeDecimal)
        {
            if (ModelState.IsValid)
            {
                db.AttributesDecimal.Add(attributeDecimal);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeDecimal.EntityId);
            return View(attributeDecimal);
        }

        // GET: AttributeDecimals/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeDecimal attributeDecimal = await db.AttributesDecimal.FindAsync(id);
            if (attributeDecimal == null)
            {
                return HttpNotFound();
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeDecimal.EntityId);
            return View(attributeDecimal);
        }

        // POST: AttributeDecimals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,MinVal,MaxVal,EntityId")] AttributeDecimal attributeDecimal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attributeDecimal).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeDecimal.EntityId);
            return View(attributeDecimal);
        }

        // GET: AttributeDecimals/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeDecimal attributeDecimal = await db.AttributesDecimal.FindAsync(id);
            if (attributeDecimal == null)
            {
                return HttpNotFound();
            }
            return View(attributeDecimal);
        }

        // POST: AttributeDecimals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AttributeDecimal attributeDecimal = await db.AttributesDecimal.FindAsync(id);
            db.AttributesDecimal.Remove(attributeDecimal);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
