﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Entities.EAV.Attributes;

namespace DynamicSearch.Controllers
{
    public class AttributeOptionsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: AttributeOptions
        public async Task<ActionResult> Index()
        {
            var attributesOption = db.AttributesOption.Include(a => a.Entity);
            return View(await attributesOption.ToListAsync());
        }

        // GET: AttributeOptions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeOption attributeOption = await db.AttributesOption.FindAsync(id);
            if (attributeOption == null)
            {
                return HttpNotFound();
            }
            return View(attributeOption);
        }

        // GET: AttributeOptions/Create
        public ActionResult Create()
        {
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name");
            return View();
        }

        // POST: AttributeOptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,EntityId")] AttributeOption attributeOption)
        {
            if (ModelState.IsValid)
            {
                db.AttributesOption.Add(attributeOption);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeOption.EntityId);
            return View(attributeOption);
        }

        // GET: AttributeOptions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeOption attributeOption = await db.AttributesOption.FindAsync(id);
            if (attributeOption == null)
            {
                return HttpNotFound();
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeOption.EntityId);
            return View(attributeOption);
        }

        // POST: AttributeOptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,EntityId")] AttributeOption attributeOption)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attributeOption).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeOption.EntityId);
            return View(attributeOption);
        }

        // GET: AttributeOptions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeOption attributeOption = await db.AttributesOption.FindAsync(id);
            if (attributeOption == null)
            {
                return HttpNotFound();
            }
            return View(attributeOption);
        }

        // POST: AttributeOptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AttributeOption attributeOption = await db.AttributesOption.FindAsync(id);
            db.AttributesOption.Remove(attributeOption);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
