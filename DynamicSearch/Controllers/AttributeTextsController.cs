﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Entities.EAV.Attributes;

namespace DynamicSearch.Controllers
{
    public class AttributeTextsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: AttributeTexts
        public async Task<ActionResult> Index()
        {
            var attributesText = db.AttributesText.Include(a => a.Entity);
            return View(await attributesText.ToListAsync());
        }

        // GET: AttributeTexts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeText attributeText = await db.AttributesText.FindAsync(id);
            if (attributeText == null)
            {
                return HttpNotFound();
            }
            return View(attributeText);
        }

        // GET: AttributeTexts/Create
        public ActionResult Create()
        {
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name");
            return View();
        }

        // POST: AttributeTexts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,EntityId")] AttributeText attributeText)
        {
            if (ModelState.IsValid)
            {
                db.AttributesText.Add(attributeText);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeText.EntityId);
            return View(attributeText);
        }

        // GET: AttributeTexts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeText attributeText = await db.AttributesText.FindAsync(id);
            if (attributeText == null)
            {
                return HttpNotFound();
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeText.EntityId);
            return View(attributeText);
        }

        // POST: AttributeTexts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,EntityId")] AttributeText attributeText)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attributeText).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeText.EntityId);
            return View(attributeText);
        }

        // GET: AttributeTexts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeText attributeText = await db.AttributesText.FindAsync(id);
            if (attributeText == null)
            {
                return HttpNotFound();
            }
            return View(attributeText);
        }

        // POST: AttributeTexts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AttributeText attributeText = await db.AttributesText.FindAsync(id);
            db.AttributesText.Remove(attributeText);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
