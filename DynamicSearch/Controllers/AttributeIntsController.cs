﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL;
using DAL.Entities.EAV.Attributes;

namespace DynamicSearch.Controllers
{
    public class AttributeIntsController : Controller
    {
        private AppDbContext db = new AppDbContext();

        // GET: AttributeInts
        public async Task<ActionResult> Index()
        {
            var attributesInt = db.AttributesInt.Include(a => a.Entity);
            return View(await attributesInt.ToListAsync());
        }

        // GET: AttributeInts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeInt attributeInt = await db.AttributesInt.FindAsync(id);
            if (attributeInt == null)
            {
                return HttpNotFound();
            }
            return View(attributeInt);
        }

        // GET: AttributeInts/Create
        public ActionResult Create()
        {
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name");
            return View();
        }

        // POST: AttributeInts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,MinVal,MaxVal,EntityId")] AttributeInt attributeInt)
        {
            if (ModelState.IsValid)
            {
                db.AttributesInt.Add(attributeInt);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeInt.EntityId);
            return View(attributeInt);
        }

        // GET: AttributeInts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeInt attributeInt = await db.AttributesInt.FindAsync(id);
            if (attributeInt == null)
            {
                return HttpNotFound();
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeInt.EntityId);
            return View(attributeInt);
        }

        // POST: AttributeInts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,MinVal,MaxVal,EntityId")] AttributeInt attributeInt)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attributeInt).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.EntityId = new SelectList(db.Entities, "Id", "Name", attributeInt.EntityId);
            return View(attributeInt);
        }

        // GET: AttributeInts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AttributeInt attributeInt = await db.AttributesInt.FindAsync(id);
            if (attributeInt == null)
            {
                return HttpNotFound();
            }
            return View(attributeInt);
        }

        // POST: AttributeInts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AttributeInt attributeInt = await db.AttributesInt.FindAsync(id);
            db.AttributesInt.Remove(attributeInt);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
